#include <Servo.h>
#include <require_cpp11.h>
#include <MFRC522.h>
#include <deprecated.h>
#include <MFRC522Extended.h>
#include <SPI.h>      // SPI device communication library.
#include <EEPROM.h>   // EEPROM (memory) library.

#define pinRST 9      // Defines pins for RST, SS conncetions respectively.
#define pinSS 10

int buzzer = 8;
int ledgreen = 7;
int ledred = 6;
Servo servo1;
int servoPos1 = 100;
int incomingByte = 0;
char a;


byte readCard[8];     // Array that will hold UID of the RFID card.
int successRead;

MFRC522 mfrc522(pinSS, pinRST);   // Creates MFRC522 instance.
MFRC522::MIFARE_Key key;          // Creates MIFARE key instance.

void setup()
{
  Serial.begin(9600); // Starts the serial connection at 9600 baud rate.
  SPI.begin();        // Initiates SPI connection between RFID module and Arduino.
  mfrc522.PCD_Init(); // Initiates MFRC522 RFID module.
  pinMode(buzzer, OUTPUT);
  pinMode(ledgreen, OUTPUT);
  servo1.attach(A0);

  do {
    successRead = getID();     // Loops getID library function until reading process is done.
  }

  while (!successRead);
  for ( int i = 0; i < mfrc522.uid.size; i++ )  // You can add multiple cards to read in the for loop.
  {
    EEPROM.write(i, readCard[i] );     // Saves RFID cards UID to EEPROM.
  }
}

void loop()
{ 
  getID();
  rotateServo();
}


int getID() // Function that will read and print the RFID cards UID.
{
  if ( ! mfrc522.PICC_IsNewCardPresent())  // If statement that looks for new cards.
  {
    return;
  }

  if ( ! mfrc522.PICC_ReadCardSerial())    // If statement that selects one of the cards.
  {
    return;
  }
  tone(buzzer, 2300);   // Send 2.3KHz sound signal...
  delay(150);           // ...for 1 sec
  noTone(buzzer);       // Stop sound...

  for (int i = 0; i < mfrc522.uid.size; i++) {
    readCard[i] = mfrc522.uid.uidByte[i];   // Reads RFID cards UID.
    Serial.print(readCard[i], HEX);         // Prints RFID cards UID to the serial monitor.
    digitalWrite(ledgreen, HIGH);
    delay(150);
    digitalWrite(ledgreen, LOW);
  }
  Serial.println();
  mfrc522.PICC_HaltA();     // Stops the reading process.
}

void rotateServo() {
  a = Serial.read();
  if (a=='a') {
    for (servoPos1 = 100; servoPos1 > 0; servoPos1--) //When servo is a position 180, servo will rotate to 0 degrees (beginning position)
      {
        servo1.write(servoPos1);
        delay(10);
      }
      delay(1000);
  
     for (servoPos1 = 0; servoPos1 < 100; servoPos1++) //When servo is a position 0, servo will rotate to 180 degrees (half rotation)
      {
        servo1.write(servoPos1);
        delay(10);
      };
  } else if (a=='b') {
    digitalWrite(ledred, HIGH);
    delay(150);
    digitalWrite(ledred, LOW);
  }
}
